<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<ul id="menu">
	<li><a href="<c:url value='/search' />" id="menu_Search"><spring:message code="menu.search" /></a></li>
	<sec:authorize ifAnyGranted="ROLE_ADMIN">
	<li><a href="<c:url value='/addForm' />" id="menu_Add"><spring:message code="menu.add" /></a></li>
	<li><a href="<c:url value='/admin/clearData' />" id="menu_ClearData"><spring:message code="menu.delete" /></a></li>
	</sec:authorize>
	<li><a href="<c:url value='/logout' />" id="menu_logOut"><spring:message code="menu.logout" /></a></li>
</ul>
<span style="float: right" id="langButtons">
    <a href="?lang=en"><img src="<c:url value='/static/en.gif' />"></a> |
    <a href="?lang=et"><img src="<c:url value='/static/et.gif' />"></a>
</span>
<br />
<br />
<br />