<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:url value="/" var="base" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Part 5</title>

<style type="text/css">
@import url("<c:url value='/static/style.css' />");
</style>
</head>
<body>
	<%@ include file="menu.jsp"%>>
	<form:form method="post" action="view" modelAttribute="viewForm">
		<table class="formTable" id="formTable">
			<tbody>
				<tr>
					<td><spring:message code="form.name" />:</td>
					<td><form:input name="name" id="nameBox" disabled="${'true'}"
							path="unit.name" /></td>
				</tr>
				<tr>
					<td><spring:message code="form.code" />:</td>
					<td><form:input name="code" id="codeBox" disabled="${'true'}"
							path="unit.code" /></td>
				</tr>
				<tr>
					<td><spring:message code="form.superunit" />:</td>

					<td><form:select id="superUnitCode" name="superUnitCode"
							disabled="${'true'}" path="">
							<form:option selected="selected" value="${superUnit.code}">${superUnit.name}</form:option>
						</form:select></td>
				</tr>
				<tr>
					<td><spring:message code="form.subunits" /></td>
					<td align="left" colspan="1"><c:forEach var="subUnit"
							items="${subUnits}">
							<span id="sub_${subUnit.code}">${subUnit.code}</span>
						</c:forEach></td>
				</tr>
				<tr>
					<td><spring:message code="form.channels" />:</td>
					<td></td>
				</tr>

				<c:forEach items="${viewForm.channels}" varStatus="status">
					<tr>
						<td></td>
						<td><form:input path="channels[${status.index}].id"
								id="channels${status.index}.id" type="hidden" /><br /> <form:select
								id="channels${status.index}.type"
								path="channels[${status.index}].type" disabled="${'true'}">
								<form:options items="${viewForm.map}" />
							</form:select> <form:input disabled="${'true'}"
								id="channels${status.index}.value" type="text"
								path="channels[${status.index}].number" /></td>
					</tr>
				</c:forEach>
				<tr>
					<td colspan="2" align="right"></td>
				</tr>
				<tr>
					<td colspan="2" align="right"><br /> <a href="../search"
						id="backLink"><spring:message code="form.back" /></a></td>
				</tr>
			</tbody>
		</table>
	</form:form>
</body>
</html>