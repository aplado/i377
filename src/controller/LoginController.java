package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
	
    @RequestMapping("/loginfailed")
    public String loginerror(ModelMap model) {
        model.addAttribute("error", "message.loginError");
        return "login";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }
}
