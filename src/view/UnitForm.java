package view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections4.Factory;
import org.apache.commons.collections4.list.LazyList;
import org.apache.commons.collections4.map.HashedMap;

import model.Channel;
import model.Unit;

public class UnitForm {
	
	@NotNull
	@Valid
	private Unit unit;
	
	private Map<String, String> units;
	private String superUnitCode;
	private String code;
	private String searchString;
	private List<Unit> subUnits;
	private String super_unit_id;
	private String addChannelButton;
	private static Map<String,String>map = new HashedMap<String,String>();

	private Factory<Channel> factory = new Factory<Channel>() {
		public Channel create() {
			return new Channel();
		}
	};
	
	@Valid
	private List<Channel> channels = LazyList.lazyList(
			new ArrayList<Channel>(), factory);

	private Unit superUnit;

	public Channel getChannelWithDeletePressed() {
		for (Channel channel : getChannels()) {
			if (channel.getDeleteButton() != null)
				return channel;
		}
		return null;
	}
	
    public void removeChannel(Channel channel) {
        getChannels().remove(channel);
    }

	public Map<String, String> getUnits() {
		return units;
	}

	public void setUnits(Map<String, String> units) {
		this.units = units;
	}

	public String getSuperUnitCode() {
		return superUnitCode;
	}

	public void setSuperUnitCode(String superUnitCode) {
		this.superUnitCode = superUnitCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public List<Unit> getSubUnits() {
		return subUnits;
	}

	public void setSubUnits(List<Unit> subUnits) {
		this.subUnits = subUnits;
	}

	public String getSuper_unit_id() {
		return super_unit_id;
	}

	public void setSuper_unit_id(String super_unit_id) {
		this.super_unit_id = super_unit_id;
	}

	public String getAddChannelButton() {
		return addChannelButton;
	}

	public void setAddChannelButton(String addChannelButton) {
		this.addChannelButton = addChannelButton;
	}

	public Unit getSuperUnit() {
		return superUnit;
	}

	public void setSuperUnit(Unit superUnit) {
		this.superUnit = superUnit;
	}

	public List<Channel> getChannels() {
		return channels;
	}

	public void setChannels(List<Channel> channels) {
		this.channels = channels;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public Map<String,String> getMap() {
		return map;
	}

	public void setMap(Map<String,String> map) {
		UnitForm.map = map;
	}

}